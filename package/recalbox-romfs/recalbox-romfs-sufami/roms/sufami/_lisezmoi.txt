## RECALBOX - SYSTEM SUFAMI ##

Placez ici vos roms sufami turbo.

Les roms doivent avoir l'extension ".st/.smc/.sfc/.fig/.zip"

Ce système supporte également les roms compressées au format .zip.
Attention toutefois, il ne s'agit que d'une archive. Les fichiers contenus dans les .zip doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip ne doit contenir qu'une seule rom compressée.